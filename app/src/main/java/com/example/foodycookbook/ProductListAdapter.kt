package com.example.foodycookbook

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.cardview_items.view.*
import kotlinx.android.synthetic.main.list_item.view.*


class ProductListAdapter(private val data: MealsList) : RecyclerView.Adapter<ProductListAdapter.MyViewHolder>() {
    class MyViewHolder(val view: View): RecyclerView.ViewHolder(view) {

        fun bind(productData: MealsListItem) {
           view.product_title_id.text = productData.strMeal
//            view.Category.text =  "₹"+productData.strCategory

            Glide.with(view.context).load(productData.strMealThumb).centerCrop().into(view.product_img_id)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return MyViewHolder(v)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(data.data[position])
    }

    override fun getItemCount(): Int {
        return data.data.size
    }
}
