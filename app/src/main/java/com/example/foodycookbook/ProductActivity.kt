package com.example.foodycookbook

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_product.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductActivity : AppCompatActivity() {

    val builder = GsonBuilder()
    val gson = builder.serializeNulls().create()
    private lateinit var productList: Array<MealsListItem>
    private lateinit var recyclerView: RecyclerView
    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var myAdapter: RecyclerView.Adapter<*>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        manager = GridLayoutManager(this, 2)
        RandomaaFood()

    }

    fun RandomaaFood() {
        RetrofitObject.instance.random().enqueue(object : Callback<JsonObject> {
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    if (response.code() == 200) {
                        var res = gson.fromJson(response.body().toString(), MealsList::class.java)
                        productList = res.data.toTypedArray().clone()
                        myAdapter = ProductListAdapter(res)

                        productRecycler.apply {
                            myAdapter = ProductListAdapter(res)
                            layoutManager = manager
                            adapter = myAdapter
                        }
                    }
                }
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                }
        }
            )
    }
}